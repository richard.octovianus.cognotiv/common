package response

import (
	"context"
	"fmt"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/cognotiv-main/common/helper"
	"gitlab.com/cognotiv-main/common/ierr"
	"go.opentelemetry.io/otel/trace"
)

type Response struct {
	Data             interface{} `json:"data"`
	Message          string      `json:"message"`
	MoreInfo         string      `json:"more_info,omitempty"`
	Code             int64       `json:"code,omitempty"`
	ValidationErrors []string    `json:"validation_errors,omitempty"`
}

func getValidation(validationErr validator.ValidationErrors) []string {
	var result []string
	if len(validationErr) == 0 {
		return nil
	}
	for _, v := range validationErr {
		switch v.Tag() {
		case "required":
			result = append(result, fmt.Sprintf("%s is required", v.Field()))
		case "eqfield":
			result = append(result, fmt.Sprintf("%s not same", v.Field()))
		default:
			result = append(result, fmt.Sprintf("%s %s", v.Field(), v.Error()))
		}
	}
	return result
}

func HttpResponse(c *fiber.Ctx, ctxOtel context.Context, data interface{}, err error, validationErr validator.ValidationErrors) error {
	var (
		message         string
		splittedMessage []string
		requestid       string
	)
	if err != nil {
		message = err.Error()
		splittedMessage = strings.Split(message, ierr.Separator)
	}
	response := Response{
		Data:             data,
		Message:          message,
		ValidationErrors: getValidation(validationErr),
	}
	if len(splittedMessage) >= 3 {
		response.Code = helper.ToInteger64(splittedMessage[0])
		response.Message = splittedMessage[1]
		response.MoreInfo = splittedMessage[2]
	}
	span := trace.SpanFromContext(ctxOtel)
	requestid = span.SpanContext().TraceID().String()
	c.Response().Header.Add("request-id", requestid)
	return c.JSON(response)
}
