package validation

import (
	"time"

	"github.com/go-playground/validator/v10"
)

func RegisterCognotivValidator(v *validator.Validate) {
	v.RegisterValidation("datetime", dateValidation, true)
}

func dateValidation(fl validator.FieldLevel) bool {
	s := fl.Field().String()
	_, err := time.Parse(time.RFC3339Nano, s)
	return err == nil
}
