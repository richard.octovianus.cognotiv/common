package ierr

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/cognotiv-main/common/helper"

	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

const (
	Separator string = "&&&"
)

type IErr interface {
	Error() string
	GetCode() int
	GetMsg() string
	GetMoreInfo() string
}

type Error struct {
	Code     int             `json:"code"`
	Msg      string          `json:"msg"`
	MoreInfo string          `json:"more_info"`
	ErrArgs  []interface{}   `json:"-"`
	Locale   string          `json:"-"`
	Context  context.Context `json:"-"`
}

func New(ctx context.Context, code int, moreInfo string, errArgs ...interface{}) *Error {
	var (
		localeString string
		locale       interface{}
	)
	locale = ctx.Value("locale")
	if locale == nil {
		localeString = "en-US"
	} else {
		localeString = locale.(string)
	}
	err := &Error{
		Code:     code,
		MoreInfo: moreInfo,
		ErrArgs:  errArgs,
		Context:  ctx,
		Locale:   localeString,
	}
	return err
}

func GetTemplate(lang, key string, args ...interface{}) string {
	tag := language.MustParse(lang)
	p := message.NewPrinter(tag)
	s := p.Sprintf(key, args...)
	if count := strings.Count(s, "MISSING"); count != 0 {
		for i := 0; i < count; i++ {
			args = append(args, "")
		}
	}
	s = p.Sprintf(key, args...)
	return s
}

func (e *Error) getLocale() string {
	switch e.Locale {
	case "en-US":
		return GetTemplate("en-US", fmt.Sprintf("%d", e.Code), e.ErrArgs...)
	case "id-ID":
		return GetTemplate("id-ID", fmt.Sprintf("%d", e.Code), e.ErrArgs...)
	default:
		return GetTemplate("id-ID", fmt.Sprintf("%d", e.Code), e.ErrArgs...)
	}
}

func (e *Error) SetLocale(locale string) {
	e.Locale = locale
}

func (e *Error) GetCode() int {
	return e.Code
}
func (e *Error) GetMsg() string {
	return e.getLocale()
}
func (e *Error) GetMoreInfo() string {
	return e.MoreInfo
}

func (e *Error) Error() string {
	str := strconv.Itoa(e.Code) + Separator + e.getLocale() + Separator + e.MoreInfo
	return str
}

func (e *Error) GetMessage() string {
	str := e.getLocale()
	return str
}

func FromString(s string) Error {
	if strings.Contains(s, "rpc error: code = Unknown desc = ") {
		s = strings.ReplaceAll(s, "rpc error: code = Unknown desc = ", "")
	}
	errDetails := strings.Split(s, Separator)
	var message, moreInfo string
	var code int
	if len(errDetails) >= 3 {
		code = helper.ToInteger(errDetails[0])
		message = errDetails[1]
		moreInfo = errDetails[2]
	} else {
		code = GENERAL_ERROR
		moreInfo = errDetails[0]
	}
	return Error{
		Code:     code,
		Msg:      message,
		MoreInfo: moreInfo,
	}
}
