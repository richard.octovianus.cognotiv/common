package ierr

const (
	//0-999 standard error
	OK                                 = 0
	GENERAL_ERROR                      = 1
	CONNECTION_RPC_ERROR               = 2
	MARSHALLING_FAILED                 = 3
	CANNOT_MAKE_CONNECTION_TO_SERVICES = 100
	VALIDATION_FAILED                  = 400
	UNAUTHORIZED                       = 401
	NOT_SUBSCRIBE                      = 402
	USER_ADMIN_NOT_FOUND               = 403

	//1000-1999 database related error
	LIST_DB_FAIL               = 1000
	INSERT_DB_FAIL             = 1001
	UPDATE_DB_FAIL             = 1002
	DELETE_DB_FAIL             = 1003
	INSERT_STORAGE_SERVER_FAIL = 1004
	GET_STORAGE_SERVER_FAIL    = 1005

	//2000-2999 NSQ related error
	NSQ_INSERT_FAIL = 2000

	//3000-3999 Payment Gateway
	PAYMENT_GATEWAY_INSERT_FAILED = 3000
	PAYMENT_GATEWAY_MARSHALL      = 3001

	//4000-4999 License
	GENERAL_LICENSE = 4000
)
