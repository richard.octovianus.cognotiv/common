package ierr

import (
	"strconv"

	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

func init() {
	message.SetString(language.Indonesian, strconv.Itoa(GENERAL_ERROR), "Harap hubungi admin")
	message.SetString(language.Indonesian, strconv.Itoa(CONNECTION_RPC_ERROR), "Koneksi RPC gagal")
	message.SetString(language.Indonesian, strconv.Itoa(MARSHALLING_FAILED), "Gagal untuk Unmarshall")
	message.SetString(language.Indonesian, strconv.Itoa(CANNOT_MAKE_CONNECTION_TO_SERVICES), "Membuat koneksi RPC gagal")
	message.SetString(language.Indonesian, strconv.Itoa(VALIDATION_FAILED), "Validasi gagal")
	message.SetString(language.Indonesian, strconv.Itoa(UNAUTHORIZED), "Unauthorized")
	message.SetString(language.Indonesian, strconv.Itoa(NOT_SUBSCRIBE), "Tidak ada langganan")
	message.SetString(language.Indonesian, strconv.Itoa(LIST_DB_FAIL), "Gagal ambil data dari database")
	message.SetString(language.Indonesian, strconv.Itoa(INSERT_DB_FAIL), "Gagal masukan data ke database")
	message.SetString(language.Indonesian, strconv.Itoa(UPDATE_DB_FAIL), "Gagal ubah data di database")
	message.SetString(language.Indonesian, strconv.Itoa(DELETE_DB_FAIL), "Gagal hapus data di database")
	message.SetString(language.Indonesian, strconv.Itoa(INSERT_STORAGE_SERVER_FAIL), "Gagal masukan data ke storage server")
	message.SetString(language.Indonesian, strconv.Itoa(GET_STORAGE_SERVER_FAIL), "Gagal mengambil data dari storage server")
	message.SetString(language.Indonesian, strconv.Itoa(PAYMENT_GATEWAY_INSERT_FAILED), "Failed insert to PG")
	message.SetString(language.Indonesian, strconv.Itoa(PAYMENT_GATEWAY_MARSHALL), "Failed marshall response from PG")
	message.SetString(language.Indonesian, strconv.Itoa(GENERAL_LICENSE), "General License Problem")
}
