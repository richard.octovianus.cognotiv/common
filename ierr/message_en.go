package ierr

import (
	"strconv"

	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

func init() {
	message.SetString(language.AmericanEnglish, strconv.Itoa(GENERAL_ERROR), "Please contact admin")
	message.SetString(language.AmericanEnglish, strconv.Itoa(CONNECTION_RPC_ERROR), "Connection RPC Failed")
	message.SetString(language.AmericanEnglish, strconv.Itoa(MARSHALLING_FAILED), "Failed To Unmarshall")
	message.SetString(language.AmericanEnglish, strconv.Itoa(CANNOT_MAKE_CONNECTION_TO_SERVICES), "Cannot make connection to serivces")
	message.SetString(language.AmericanEnglish, strconv.Itoa(VALIDATION_FAILED), "Validation Failed")
	message.SetString(language.AmericanEnglish, strconv.Itoa(UNAUTHORIZED), "Unauthorized")
	message.SetString(language.AmericanEnglish, strconv.Itoa(NOT_SUBSCRIBE), "Not Subscribe")
	message.SetString(language.AmericanEnglish, strconv.Itoa(LIST_DB_FAIL), "Failed getting data from DB")
	message.SetString(language.AmericanEnglish, strconv.Itoa(INSERT_DB_FAIL), "Failed insert data to DB")
	message.SetString(language.AmericanEnglish, strconv.Itoa(UPDATE_DB_FAIL), "Failed modify data to DB")
	message.SetString(language.AmericanEnglish, strconv.Itoa(DELETE_DB_FAIL), "Failed delete data from DB")
	message.SetString(language.AmericanEnglish, strconv.Itoa(INSERT_STORAGE_SERVER_FAIL), "Failed insert data to Storage Server")
	message.SetString(language.AmericanEnglish, strconv.Itoa(GET_STORAGE_SERVER_FAIL), "Failed get data from Storage Server")
	message.SetString(language.AmericanEnglish, strconv.Itoa(PAYMENT_GATEWAY_INSERT_FAILED), "Failed insert to PG")
	message.SetString(language.AmericanEnglish, strconv.Itoa(PAYMENT_GATEWAY_MARSHALL), "Failed marshall response from PG")
	message.SetString(language.AmericanEnglish, strconv.Itoa(GENERAL_LICENSE), "General License Problem")
}
