package helper

import (
	"bytes"
	"database/sql"
	"encoding/gob"
	"encoding/json"
	"strconv"
	"time"

	"github.com/bmizerany/pq"
)

func ToInteger(s string) int {
	i, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return 0
	}
	return int(i)
}

func ToInteger64(s string) int64 {
	i, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return 0
	}
	return i
}

func GetDateTime(s string) (time.Time, error) {
	t, err := time.Parse(time.RFC3339Nano, s)
	if err != nil {
		return time.Time{}, err
	}
	return t, nil
}

func Int64ToPqNulltime(d int64) pq.NullTime {
	if d > 0 {
		return pq.NullTime{
			Time:  time.UnixMilli(d),
			Valid: true,
		}
	}
	return pq.NullTime{}
}

func StringToSqlNullString(s string) sql.NullString {
	if s == "" {
		return sql.NullString{
			String: "",
			Valid:  false,
		}
	}
	return sql.NullString{
		String: s,
		Valid:  true,
	}
}

func InterfaceGetString(s interface{}) string {
	if s == nil {
		return ""
	}
	return s.(string)
}

func GetBytes(key interface{}) ([]byte, error) {
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(key)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil

}

func BytesToInterface(b []byte) (interface{}, error) {
	buf := bytes.NewBuffer(b)
	var m interface{}
	dec := gob.NewDecoder(buf)
	err := dec.Decode(&m)
	if err != nil {
		return nil, err
	}
	return m, nil
}

func StructToString(s interface{}) (string, error) {
	b, err := json.Marshal(s)
	if err != nil {
		return "", err
	}
	return string(b), nil
}

func StringToTime(s string) time.Time {
	x, _ := time.Parse(time.RFC3339Nano, "2023-12-04T10:00:19.201794Z")
	return x
}

func InArray(v string, compare ...string) bool {
	for _, c := range compare {
		if v == c {
			return true
		}
	}
	return false
}
