package middleware

import (
	"context"
	"errors"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
	response "gitlab.com/cognotiv-main/common/response"
	pandailmspb "gitlab.com/cognotiv-r2-002/pandailms-proto"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	tracesdk "go.opentelemetry.io/otel/sdk/trace"
	"google.golang.org/grpc"
)

type Middleware struct {
	Target  string
	Tracer  *tracesdk.TracerProvider
	AppName string
	Log     *logrus.Logger
}

func NewMiddleware(target string, tracer *tracesdk.TracerProvider, appName string, log *logrus.Logger) *Middleware {
	return &Middleware{
		Target:  target,
		Tracer:  tracer,
		AppName: appName,
		Log:     log,
	}
}

func (m *Middleware) Authorized(c *fiber.Ctx) error {
	var (
		token       []byte = c.Request().Header.Peek("token")
		accessKey   []byte = c.Request().Header.Peek("access_key")
		tokenString        = string(token)
		req         *pandailmspb.AuthorizedRequest
	)
	if tokenString == "" {
		tokenString = c.Cookies("token", "")
	}
	req = &pandailmspb.AuthorizedRequest{Token: tokenString, AccessKey: string(accessKey)}
	ctx, _ := m.Tracer.Tracer(m.AppName).Start(c.Context(), "Authorize")
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	if len(tokenString) == 0 {
		return response.HttpResponse(c, ctx, nil, errors.New("unauthorized"), nil)
	}

	conn, err := grpc.Dial(m.Target,
		grpc.WithInsecure(),
		grpc.WithUnaryInterceptor(otelgrpc.UnaryClientInterceptor()),
		grpc.WithStreamInterceptor(otelgrpc.StreamClientInterceptor()),
	)
	if err != nil {
		panic(err)
	}
	providerpb := pandailmspb.NewPandaiLmsClient(conn)

	resp, err := providerpb.Authorized(ctx, req)
	if err != nil || (resp != nil && !resp.Authenticated) {
		m.Log.WithContext(ctx).WithFields(logrus.Fields{
			"req":  req,
			"resp": resp,
		}).Error(err, "Authorized.m.uc.Authorized")
		return response.HttpResponse(c, ctx, nil, errors.New("unauthorized"), nil)
	}
	c.Locals("user_uuid", resp.UserUuid)
	return c.Next()
}

// allow if no user
func (m *Middleware) UserChecker(c *fiber.Ctx) error {
	var (
		token       []byte = c.Request().Header.Peek("token")
		accessKey   []byte = c.Request().Header.Peek("access_key")
		tokenString        = string(token)
		req         *pandailmspb.AuthorizedRequest
	)
	if tokenString == "" {
		tokenString = c.Cookies("token", "")
	}
	req = &pandailmspb.AuthorizedRequest{Token: tokenString, AccessKey: string(accessKey)}
	ctx, _ := m.Tracer.Tracer(m.AppName).Start(c.Context(), "Authorize")
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	conn, err := grpc.Dial(m.Target,
		grpc.WithInsecure(),
		grpc.WithUnaryInterceptor(otelgrpc.UnaryClientInterceptor()),
		grpc.WithStreamInterceptor(otelgrpc.StreamClientInterceptor()),
	)
	if err != nil {
		panic(err)
	}
	providerpb := pandailmspb.NewPandaiLmsClient(conn)

	resp, err := providerpb.Authorized(ctx, req)
	if err != nil || (resp != nil && !resp.Authenticated) {
		//ignore
		return c.Next()
	}
	c.Locals("user_uuid", resp.UserUuid)
	return c.Next()
}
