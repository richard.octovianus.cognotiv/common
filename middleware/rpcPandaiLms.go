package middleware

import (
	"context"

	_ "github.com/mbobakov/grpc-consul-resolver"
	pandailmspb "gitlab.com/cognotiv-r2-002/pandailms-proto"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"google.golang.org/grpc"
)

type RpcDocument struct {
	provider pandailmspb.PandaiLmsClient
	conn     *grpc.ClientConn
}

type UserRepoInterface interface {
	Authorized(ctx context.Context, req *pandailmspb.AuthorizedRequest) (*pandailmspb.AuthorizedResponse, error)
	RefreshToken(ctx context.Context, req *pandailmspb.RefreshTokenRequest) (*pandailmspb.LoginResponse, error)
}

func (m *RpcDocument) Authorized(ctx context.Context, req *pandailmspb.AuthorizedRequest) (*pandailmspb.AuthorizedResponse, error) {
	return m.provider.Authorized(ctx, req)
}

func (m *RpcDocument) RefreshToken(ctx context.Context, req *pandailmspb.RefreshTokenRequest) (*pandailmspb.LoginResponse, error) {
	return m.provider.RefreshToken(ctx, req)
}

func (m *RpcDocument) Close(ctx context.Context) error {
	return m.conn.Close()
}

func NewUserRepo(target string) (UserRepoInterface, error) {
	conn, err := grpc.Dial(target,
		grpc.WithInsecure(),
		grpc.WithUnaryInterceptor(otelgrpc.UnaryClientInterceptor()),
		grpc.WithStreamInterceptor(otelgrpc.StreamClientInterceptor()),
	)
	if err != nil {
		panic(err)
	}
	providerpb := pandailmspb.NewPandaiLmsClient(conn)
	return &RpcDocument{
		provider: providerpb,
		conn:     conn,
	}, nil
}
