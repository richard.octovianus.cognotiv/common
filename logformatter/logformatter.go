package logformatter

import (
	log "github.com/sirupsen/logrus"
	"go.opentelemetry.io/otel/trace"
)

type CustomLogger struct {
	formatter log.JSONFormatter
}

func (l *CustomLogger) SetTheFormatter(formatter log.JSONFormatter) {
	l.formatter = formatter
}

func (l CustomLogger) Format(entry *log.Entry) ([]byte, error) {
	span := trace.SpanFromContext(entry.Context)
	entry.Data["trace_id"] = span.SpanContext().TraceID().String()
	entry.Data["span_id"] = span.SpanContext().SpanID().String()
	return l.formatter.Format(entry)
}
